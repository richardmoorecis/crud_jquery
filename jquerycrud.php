<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

</head>

<body>
    <div class="container">
        <h1 class="text-primary text-uppercase text-center">Crud</h1>

        <div class="d-flex justify-content-end">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Add </button>
        </div>
        <div id="records_contant">


        </div>
        <div class="modal" id="myModal">
            <div class="modal-dialog">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">ADD ELEMENT</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" name="" id="name" class="form-control" placeholder="enter name">
                        </div>

                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" name="email" id="email" class="form-control" placeholder="enter email">
                        </div>

                        <div class="form-group">
                            <label>Mobile</label>
                            <input type="number" name="mobile" id="mobile" class="form-control" placeholder="enter mobile">
                        </div>

                        <div class="form-group">
                            <label>Skills</label>
                            <input type="text" name="skill" id="skill" class="form-control" placeholder="enter skill">
                        </div>
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="addRecord()">Add</button>
                    </div>

                </div>
            </div>
        </div>
        <!-- update modal -->

        <!--  The Modal -->
        <div class="modal" id="update_usermodal">
            <div class="modal-dialog">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">EDIT DATA</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" name="" id="update_name" class="form-control" placeholder="enter name">
                        </div>

                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" name="" id="update_email" class="form-control" placeholder="enter email">
                        </div>

                        <div class="form-group">
                            <label>Mobile</label>
                            <input type="number" name="" id="update_mobile" class="form-control" placeholder="enter mobile">
                        </div>
                        <div class="form-group">
                            <label>Skills</label>
                            <input type="text" name="" id="update_skill" class="form-control" placeholder="enter skills">
                        </div>
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="updateuserdetail()">Update</button>
                        <input type="hidden" id="hidden_user_id">
                    </div>

                </div>
            </div>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            readRecords();
        });

        function readRecords() {
            var readrecord = "readrecord";
            $.ajax({
                url: "display.php",
                type: "post",
                data: {
                    readrecord: readrecord
                },
                success: function(data, status) {
                    $('#records_contant').html(data);
                }
            });
        }

        function addRecord() {
            var name = $('#name').val();
            var email = $('#email').val();
            var mobile = $('#mobile').val();
            var skill = $('#skill').val();

            $.ajax({
                url: "display.php",
                type: "post",
                data: {
                    name: name,
                    email: email,
                    mobile: mobile,
                    skill: skill
                },
                success: function(data, status) {
                    readRecords();
                }
            });

        }
        //deleting the record
        function DeleteUser(deleteid) {
            var conf = confirm("Are you sure");
            if (conf == true) {
                $.ajax({
                    url: "display.php",
                    type: "post",
                    data: {
                        deleteid: deleteid
                    },
                    success: function(data, status) {
                        readRecords();
                    }
                });
            }
        }

        function GetUserDetails(id) {
            $('#hidden_user_id').val(id);

            $.post("display.php", {
                id: id
            }, function(data, status) {
                var user = JSON.parse(data);
                $('#update_name').val(user.name);
                $('#update_email').val(user.email);
                $('#update_mobile').val(user.mobile);
                $('#update_skill').val(user.skill);
            });
            $('#update_usermodal').modal("show");

        }

        function updateuserdetail(id) {
            var nameupd = $('#update_name').val();
            var emailupd = $('#update_email').val();
            var mobileupd = $('#update_mobile').val();
            var skillupd = $('#update_skill').val();
            var hidden_user_idupd = $('#hidden_user_id').val();

            $.post("display.php", {
                    hidden_user_id: hidden_user_idupd,
                    name: nameupd,
                    email: emailupd,
                    mobile: mobileupd,
                    skill: skillupd,
                },
                function(data, status) {
                    $('#update_usermodal').modal("hide");
                    readRecords();
                }
            )
        }
    </script>
</body>


</html>